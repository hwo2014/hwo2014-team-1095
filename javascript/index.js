var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm ", botName, ", and connect to ", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function () {
  return send({
    msgType: "join",
    data:    {
      name: botName,
      key:  botKey
    }
  });
});

function send(json) {
  console.log("Sent ", JSON.stringify(json));
  client.write(JSON.stringify(json));
  return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

var pieces = []
jsonStream.on('data', function (data) {
  console.log(JSON.stringify(data))
  if (data.msgType === 'carPositions') {
    var angle = data.data[0].angle
    var pieceIndex = data.data[0].piecePosition.pieceIndex
    var inPieceDistance = data.data[0].piecePosition.inPieceDistance
    console.log("Angle ", angle)
    console.log("piece ", pieces[pieceIndex])

    send({
      msgType: "throttle",
      data:    getSpeedForAngleAndPiece(angle, pieceIndex, inPieceDistance)
    });
  } else if (data.msgType === 'gameInit') {
    pieces = data.data.race.track.pieces
  } else if (data.msgType === 'crash') {
    console.log("!!!!!!! CRASHED !!!!!!!")
  } else {
    if (data.msgType === 'join') {
      console.log('Joined')
    } else if (data.msgType === 'gameStart') {
      console.log('Race started');
    } else if (data.msgType === 'gameEnd') {
      console.log('Race ended');
    }
    send({
      msgType: "ping",
      data:    {}
    });
  }
});

jsonStream.on('error', function () {
  return console.log("disconnected");
});


function getSpeedForAngleAndPiece(angle, pieceIdx, inPieceDistance) {
  var absAngle = Math.abs(angle)

  if (!curPiece(pieceIdx).radius && !nextPiece(pieceIdx).radius) return 0.98 //current and next pieces are strait
  if (endOfCurve(pieceIdx, inPieceDistance) && absAngle < 30) return 0.95 //full throttle at the end of curve!
  if (endOfStrait(pieceIdx, inPieceDistance) && nextPiece(pieceIdx).radius) return 0.055 //brake in the end of strait before curve!

  if (absAngle > 30) return 0.01
  if (absAngle > 20) return 0.03
  if (absAngle > 5) return 0.05
  if (absAngle > 4) return 0.1

  return 1
}

function curPiece(idx) { return pieces[idx] }

function nextPiece(idx) { return idx+1 < pieces.length ? pieces[idx+1] : pieces[0] }

function endOfStrait(idx, inPieceDistance) {
  var piece = curPiece(idx)
  var leftToTravel =  piece.length - inPieceDistance
  return !piece.radius && leftToTravel < piece.length * 0.6
}

function endOfCurve(idx, inPieceDistance) {
  var piece = curPiece(idx)
  if (!piece.radius) return false

  var curveLength = Math.abs(piece.angle)/360 * 2 * Math.PI * piece.radius
  var leftToTravel = curveLength - inPieceDistance
  console.log('curve', leftToTravel, curveLength)
  return leftToTravel < curveLength * 0.6
}
